﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiREST.Models
{
    public class SalaoContext : DbContext
    {

    public SalaoContext(DbContextOptions<SalaoContext> options)
        : base(options)
        {
        }

        public DbSet<Pessoa> Pessoas { get; set; }

        public DbSet<Agenda> Agendas { get; set; }

    }
}



