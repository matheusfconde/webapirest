﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiREST.Models
{
    public class Agenda
    {
        public long Id { get; set; }

        public string Nome { get; set; }

        public DateTime DataHora { get; set; }

        public long IdBarbeiro { get; set; }
    }
}
