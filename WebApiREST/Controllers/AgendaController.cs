﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using WebApiREST.Models;
//using System.Web.Http.Cors;


namespace WebApiREST.Controllers
{
    [Route("api/agenda")]
    [EnableCors("CorsPolicy")]
    public class AgendaController : Controller
    {
        public DateTime PegaHoraBrasilia()
        {
            return TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time"));
        }

        private readonly SalaoContext _context;

        public AgendaController(SalaoContext context)
        {
            _context = context;

            if (_context.Agendas.Count() == 0)
            {
                _context.Agendas.Add(new Agenda { Nome = "Matheus", IdBarbeiro = 1, DataHora = PegaHoraBrasilia() });
                _context.SaveChanges();
            }

        }

        [HttpGet]
        public IEnumerable<Agenda> GetAll()
        {
            return _context.Agendas.ToList();
        }

        [HttpGet("{id}", Name = "GetAgenda")]
        public IActionResult GetById(long id)
        {
            var item = _context.Agendas.FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Agenda agenda)
        {
            if (agenda == null)
            {
                return BadRequest();
            }

            _context.Agendas.Add(agenda);
            _context.SaveChanges();

            return CreatedAtRoute("GetAgenda", new { id = agenda.Id }, agenda);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Agenda agenda)
        {
            if (agenda == null || agenda.Id != id)
            {
                return BadRequest();
            }

            var todo = _context.Agendas.FirstOrDefault(t => t.Id == id);
            if (todo == null)
            {
                return NotFound();
            }

            todo.Nome = agenda.Nome;
            todo.IdBarbeiro = agenda.IdBarbeiro;
            todo.DataHora = agenda.DataHora;
  

            _context.Agendas.Update(todo);
            _context.SaveChanges();
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var todo = _context.Agendas.FirstOrDefault(t => t.Id == id);
            if (todo == null)
            {
                return NotFound();
            }

            _context.Agendas.Remove(todo);
            _context.SaveChanges();
            return new NoContentResult();
        }

    }
}


