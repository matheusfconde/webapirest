﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using WebApiREST.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApiREST.Controllers
{
    [Route("api/pessoa")]
    [EnableCors("CorsPolicy")]
    public class PessoaController : Controller
    {

        public DateTime PegaHoraBrasilia()
        {
            return TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("E. South America Standard Time"));
        }

        private readonly SalaoContext _context;

        public PessoaController(SalaoContext context)
        {
            _context = context;

            if (_context.Pessoas.Count() == 0)
            {
                _context.Pessoas.Add(new Pessoa { Nome = "KIL", Sexo = "Masculino", Situacao = true, Nascimento = PegaHoraBrasilia() });
                _context.SaveChanges();
            }

        }

        [HttpGet]
        public IEnumerable<Pessoa> GetAll()
        {
            return _context.Pessoas.ToList();
        }

        [HttpGet("{id}", Name = "GetPessoa")]
        public IActionResult GetById(long id)
        {
            var item = _context.Pessoas.FirstOrDefault(t => t.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return new ObjectResult(item);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Pessoa pessoa)
        {
            if (pessoa == null)
            {
                return BadRequest();
            }

            _context.Pessoas.Add(pessoa);
            _context.SaveChanges();

            return CreatedAtRoute("GetPessoa", new { id = pessoa.Id }, pessoa);
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] Pessoa pessoa)
        {
            if (pessoa == null || pessoa.Id != id)
            {
                return BadRequest();
            }

            var todo = _context.Pessoas.FirstOrDefault(t => t.Id == id);
            if (todo == null)
            {
                return NotFound();
            }

            todo.Nome = pessoa.Nome;
            todo.Nascimento = pessoa.Nascimento;
            todo.Sexo = pessoa.Sexo;
            todo.Situacao = pessoa.Situacao;

            _context.Pessoas.Update(todo);
            _context.SaveChanges();
            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(long id)
        {
            var todo = _context.Pessoas.FirstOrDefault(t => t.Id == id);
            if (todo == null)
            {
                return NotFound();
            }

            _context.Pessoas.Remove(todo);
            _context.SaveChanges();
            return new NoContentResult();
        }

    }

}

/*
     {
        "nome": "Felipe",
        "nascimento": "0001-01-01",
        "sexo": "Masculino",
        "situacao": true
    }
 */
